package it.unibo.oop.lab.collections1;

import java.util.*;

/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
    	
    	 /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	
    	final int INIT = 1000;
    	//final int FINAL = 2000;
    	final int TO_MS = 1_000_000;
    	
    	List<Integer> arrayList = new ArrayList<Integer>(INIT);
    	
    	for(int i=0; i<INIT;i++) {
    		arrayList.add((INIT+i));
    	}
    	
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	
    	List<Integer> linkedList = new LinkedList<Integer>(arrayList);	
       

        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	
    	int tmp = arrayList.get((arrayList.size()-1));
    	arrayList.set((arrayList.size()-1), arrayList.get(0));
    	arrayList.set(0, tmp);
    	
    	
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	
    	for(final int i : arrayList) {
    		System.out.println(i);
    	}
    	
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	long time = System.nanoTime();
    	
    	for(int i=0; i<100000; i++) {
    		arrayList.add(0, i);
    	}
    	
    	time = System.nanoTime() - time;
    	System.out.println("Inserting new elements in the head of the arrayList took " + time/TO_MS + " ms");
    	
    	
    	time = System.nanoTime();
    	
    	for(int i=0; i<100000; i++) {
    		linkedList.add(0, i);
    	}
    	
    	time = System.nanoTime() - time;
    	System.out.println("Inserting new elements in the head of the linkedList took " + time/TO_MS + " ms");
    	
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
    	
    	int middle=arrayList.size()/2;
    	time = System.nanoTime();
    	
    	for(int i=0; i<1000; i++) {
    		arrayList.get(middle);
    	}
    	
    	time=System.nanoTime() - time;
    	System.out.println("Reading elements in the middle of the arrayList took " + time/TO_MS + " ms");
    	
    	middle=linkedList.size()/2;
    	time = System.nanoTime();
    	
    	for(int i=0; i<1000; i++) {
    		linkedList.get(middle);
    	}
    	
    	time=System.nanoTime() - time;
    	System.out.println("Reading elements in the middle of the linkedList took " + time/TO_MS + " ms");
    	
    	final Map<String,Long> continents = new HashMap<>();
    	
    	continents.put("Africa", 1_110_635_000l);
    	continents.put("Americas", 972_005_000l);
    	continents.put("Antartica", 0l);
    	continents.put("Asia", 4_298_723_000l);
    	continents.put("Europe", 742_452_000l);
    	continents.put("Oceania", 38_304_000l);
    	
    	for ( String s1 : continents.keySet()) {
    		 System . out . println (" Continente : " + s1 + " Valore : " + continents.get(s1));
    	}

    	
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
        /*
         * 8) Compute the population of the world
         */
    }
}
