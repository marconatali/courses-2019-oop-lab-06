package it.unibo.oop.lab.exception1;

public class NotEnoughBatteryException extends IllegalStateException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double batteryLevel;
	private double batteryRequired;
	
	public NotEnoughBatteryException(double batteryLevel,double batteryRequired) {
		super();
		this.batteryLevel=batteryLevel;
		this.batteryRequired=batteryRequired;
	}
	
	public String toString() {
		return "Can not move the robot, it needs a recharge, battery level=" + this.batteryLevel + " battery required=" + this.batteryRequired;
	}


}
